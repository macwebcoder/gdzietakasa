'use strict';

/**
 * @ngdoc overview
 * @name gdzietakasaApp
 * @description
 * # gdzietakasaApp
 *
 * Main module of the application.
 */
angular
    .module('gdzietakasaApp', [
        'ngAnimate',
        'ngCookies',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        'ngTouch',
        'ngStorage'
    ])
    .config(function ($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'views/main.html',
                controller: 'MainCtrl'
            })
            .otherwise({
                redirectTo: '/'
            });
    }).run(function($rootScope, appFactory){

        appFactory.getConfig().
            then(function (config) {
                $rootScope.app = config.app;
            }).
            catch(function (e) {
                console.log(e);
            });

    });
