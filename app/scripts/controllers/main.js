'use strict';

/**
 * @ngdoc function
 * @name gdzietakasaApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the gdzietakasaApp
 */
angular.module('gdzietakasaApp')
    .controller('MainCtrl', function ($scope, appFactory) {

        appFactory.getTransactions().
            then(function (transactions) {
                $scope.transactions = transactions.items;
            }).
            catch(function (e) {
                console.log(e);
            });


    });
