'use strict';

/**
 * @ngdoc service
 * @name gdzietakasaApp.appFactory
 * @description
 * # appFactory
 * Factory in the gdzietakasaApp.
 */
angular.module('gdzietakasaApp')
    .factory('appFactory', function ($http, $q, apiFactory) {



        function getDataFromApi(apiName) {
            var deferred = $q.defer();
            apiFactory.getApiData(apiName).
                then(function (data) { deferred.resolve(data)}).
                catch(function(e) { deferred.reject(e) });
            return deferred.promise;
        }

        function getConfig() {
            return getDataFromApi('config');
        }

        function getTransactions() {
            return getDataFromApi('transactions');
        }

        // Public API here
        return {
            getConfig: getConfig,
            getTransactions: getTransactions
        };
    });
