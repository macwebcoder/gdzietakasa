'use strict';

/**
 * @ngdoc service
 * @name gdzietakasaApp.cacheFactory
 * @description
 * # cacheFactory
 * Factory in the gdzietakasaApp.
 */
angular.module('gdzietakasaApp')
    .factory('cacheFactory', function ($q, $localStorage) {

        function getTimestamp() {
            return Math.round(+new Date()/1000);
        }

        function isCacheSet(cacheName) {
            if ($localStorage[cacheName]) {
                return true;
            }
            return false;
        }

        function isCacheValid(cacheName) {
            if (isCacheSet(cacheName)) {
                var cache = $localStorage[cacheName],
                    currentTimestamp = getTimestamp(),
                    cacheTimestamp = cache.timestamp,
                    cacheLifetime = cache.lifetime;


                if (cacheLifetime === 0 || (cacheLifetime > 0 && currentTimestamp - cacheTimestamp <= cacheLifetime)) {
                    return true;
                }
            }
            return false;
        }

        function emptyCache(cacheName) {
            delete $localStorage[cacheName];
        }

        function emptyAllCache() {
            $localStorage.$reset();
        }

        function getCacheData(cacheName) {
            var deferred = $q.defer();

            if (isCacheValid(cacheName)) {
                deferred.resolve($localStorage[cacheName]);
            } else {
                emptyCache(cacheName);
                deferred.resolve(null);
            }

            return deferred.promise;
        }

        function putCacheData(cacheName, data, lifetime) {

            if (!cacheName || cacheName.length === 0) {
                return;
            }

            if (data === undefined) {
                data = '';
            }

            if (lifetime === undefined) {
                lifetime = 0;
            }

            $localStorage[cacheName] = {
                data: data,
                timestamp: getTimestamp(),
                lifetime: lifetime
            }
        }

        // Public API here
        return {
            getCacheData: getCacheData,
            putCacheData: putCacheData,
            emptyAllCache: emptyAllCache
        };
    });
