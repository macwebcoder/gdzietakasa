'use strict';

/**
 * @ngdoc service
 * @name gdzietakasaApp.transactionsFactory
 * @description
 * # transactionsFactory
 * Factory in the gdzietakasaApp.
 */
angular.module('gdzietakasaApp')
  .factory('transactionsFactory', function () {
    // Service logic
    // ...

    var meaningOfLife = 42;

    // Public API here
    return {
      someMethod: function () {
        return meaningOfLife;
      }
    };
  });
