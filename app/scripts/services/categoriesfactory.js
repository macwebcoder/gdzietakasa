'use strict';

/**
 * @ngdoc service
 * @name gdzietakasaApp.categoriesFactory
 * @description
 * # categoriesFactory
 * Factory in the gdzietakasaApp.
 */
angular.module('gdzietakasaApp')
  .factory('categoriesFactory', function () {
    // Service logic
    // ...

    var meaningOfLife = 42;

    // Public API here
    return {
      someMethod: function () {
        return meaningOfLife;
      }
    };
  });
