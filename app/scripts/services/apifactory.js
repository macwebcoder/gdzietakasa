'use strict';

/**
 * @ngdoc service
 * @name gdzietakasaApp.apiFactory
 * @description
 * # apiFactory
 * Factory in the gdzietakasaApp.
 */
angular.module('gdzietakasaApp')
    .factory('apiFactory', function ($http, $q, cacheFactory) {

        var api = {
            config: {
                endpoint: '/api/config.json',
                lifetime: 3600
            },
            transactions: {
                endpoint: '/api/transactions.json',
                lifetime: 0
            }
        };

        function getApiData(apiName) {
            var deferred = $q.defer();

            if (api[apiName]) {

                cacheFactory.getCacheData(apiName).then(function(cache){
                    console.log('cache response for ' + apiName, cache);
                    if (cache === null) {
                        $http.get(api[apiName].endpoint).
                            then(function (resource) {
                                console.log('api response for ' + apiName, resource.data);
                                cacheFactory.putCacheData(apiName, resource.data, api[apiName].lifetime);
                                deferred.resolve(resource.data)
                            }).
                            catch(function (resource) {
                                deferred.reject(resource.data)
                            });
                    } else {
                        deferred.resolve(cache.data);
                    }
                });

            } else {
                deferred.reject('There is no API like: ' + apiName);
            }

            return deferred.promise;
        }

        // Public API here
        return {
            getApiData: getApiData
        };
    });
