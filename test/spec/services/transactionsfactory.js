'use strict';

describe('Service: transactionsFactory', function () {

  // load the service's module
  beforeEach(module('gdzietakasaApp'));

  // instantiate service
  var transactionsFactory;
  beforeEach(inject(function (_transactionsFactory_) {
    transactionsFactory = _transactionsFactory_;
  }));

  it('should do something', function () {
    expect(!!transactionsFactory).toBe(true);
  });

});
