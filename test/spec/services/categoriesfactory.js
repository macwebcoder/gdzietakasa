'use strict';

describe('Service: categoriesFactory', function () {

  // load the service's module
  beforeEach(module('gdzietakasaApp'));

  // instantiate service
  var categoriesFactory;
  beforeEach(inject(function (_categoriesFactory_) {
    categoriesFactory = _categoriesFactory_;
  }));

  it('should do something', function () {
    expect(!!categoriesFactory).toBe(true);
  });

});
