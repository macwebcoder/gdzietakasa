'use strict';

describe('Service: appFactory', function () {

  // load the service's module
  beforeEach(module('gdzietakasaApp'));

  // instantiate service
  var appFactory;
  beforeEach(inject(function (_appFactory_) {
    appFactory = _appFactory_;
  }));

  it('should do something', function () {
    expect(!!appFactory).toBe(true);
  });

});
